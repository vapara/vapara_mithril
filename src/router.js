import { createWebHistory, createRouter } from "vue-router";
import Home from "./Views/Home.vue";
import Mentions from "./Views/mentions.vue";
import Category from "./Views/category/Category.vue";
import Product from "./Views/product/Product.vue";

const history = createWebHistory();
const routes = [
  {
    path: "/",
    component: Home,
  },
  {
    path: "/categories/:categoryId",
    component: Category,
    name: "category",
    props: true,
  },
  {
    path: "/products/:productId",
    component: Product,
    name: "product",
    props: true,
  },
  {
    path: "/mentions",
    component: Mentions,
  },
];
const router = createRouter({ history, routes });
export default router;
