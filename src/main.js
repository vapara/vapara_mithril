import { createApp } from "vue";
import App from "./App.vue";
import "./index.css";
import router from "./router";
import Store from "./store/index.js";

const app = createApp({});
createApp(App).use(Store).use(router).mount("#app");
