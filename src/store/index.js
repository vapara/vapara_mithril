import { createStore } from "vuex";
import axios from "axios";
import createPersistedState from "vuex-persistedstate";

export default createStore({
  state: {
    categories: [],
    products: [],
    product : []
  },


  mutations: {
    theProducts: (state, products) => (state.products = products),
    theCategories: (state, categories) => (state.categories = categories),
    theProd:(state,product) => (state.product = product),
  },

  actions: {
    async fetchProducts({ commit }) {
      const response = await axios.get("https://vapara.mithril.re/products");
      console.log("Affichage des produits");
      commit("theProducts", response.data);
    },
    async fetchCategories({ commit }) {
      const response = await axios.get("https://vapara.mithril.re/categories");
      console.log("Affichage des categories");
      commit("theCategories", response.data);
    },
    async getProd({commit},productId){
      const response = await axios.get(`https://vapara.mithril.re/products/${productId}`);
      console.log("Affichage d'un produit'");
      commit("theProd", response.data);
    },
    async getProdCat({commit},categoryId){
      const response = await axios.get(`https://vapara.mithril.re/categories/${categoryId}/products`);
      console.log("Affichage des produits d'une categorie");
      commit("theProducts", response.data);
    }
  },
  plugins: [createPersistedState()],
});
