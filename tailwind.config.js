module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [
    function ({ addBase, config }) {
      addBase({
        'body': { backgroundColor: '#e2e8f0' },
      })
    }],
}
